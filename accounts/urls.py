from django.urls import path

from . import views

urlpatterns = [
    path("", views.AuthTokenAPIView.as_view()),
    path("register/", views.RegisterAPIView.as_view()),
    path("me/", views.UserInfoAPIView.as_view()),
]
