from rest_framework.generics import CreateAPIView, RetrieveAPIView, get_object_or_404
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from accounts.models import User
from .serializers import UserRegisterSerializer, UserInfoSerializer


class RegisterAPIView(CreateAPIView):
    """
    Регистрация (создание нового пользователя)
    """

    serializer_class = UserRegisterSerializer


class AuthTokenAPIView(ObtainAuthToken):
    """
    Получить токен авторизации
    """

    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        return Response({"token": token.key, "email": user.email})


class UserInfoAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return get_object_or_404(self.get_queryset(), id=self.request.user.id)
