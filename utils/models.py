from django.db import models


class HitCount(models.Model):
    hits = models.IntegerField(default=0)

    def increase(self):
        self.hits += 1
        self.save()


class City(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.name
