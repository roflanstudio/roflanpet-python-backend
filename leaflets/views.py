from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from . import models, serializers


class MyLeafletsAPIView(ListAPIView):
    serializer_class = serializers.LeafletListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return models.Leaflet.objects.filter(creator=self.request.user)


class LeafletsCreateAPIView(CreateAPIView):
    serializer_class = serializers.LeafletCreateSerializer


class MainLeafletAPIView(APIView):
    def get(self, request, *args, **kwargs):
        return Response({"count": models.Leaflet.objects.filter(resolved=True).count()})


class LeafletCounterView(RetrieveAPIView):
    queryset = models.Leaflet.objects.all()
    serializer_class = serializers.LeafletListSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.hit_counter.increase()
        return Response({"ok": True})


class LeafletQRCounterView(RetrieveAPIView):
    queryset = models.Leaflet.objects.all()
    serializer_class = serializers.LeafletListSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.qr_counter.increase()
        return Response({"ok": True})
