from django.urls import path

from . import views

urlpatterns = [
    path("my/", views.MyLeafletsAPIView.as_view()),
    path("main/", views.MainLeafletAPIView.as_view()),
    path("create/", views.LeafletsCreateAPIView.as_view()),
    path("hit/<int:pk>/", views.LeafletCounterView.as_view()),
    path("hit_qr/<int:pk>/", views.LeafletQRCounterView.as_view()),
]
