from django.core.mail import send_mail
from rest_framework import serializers


from pets.serializers import PetCreateSerializer, PetSerializer
from .models import Leaflet
from accounts.models import User
from pets.models import Pet


class LeafletListSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source="get_status_display")
    hit_counter = serializers.IntegerField(source="hit_counter.hits")
    qr_counter = serializers.IntegerField(source="qr_counter.hits")
    pet = PetSerializer()
    city = serializers.SerializerMethodField()

    class Meta:
        model = Leaflet
        fields = [
            "id",
            "status",
            "pet",
            "resolved",
            "city",
            "hit_counter",
            "qr_counter",
        ]

    def get_city(self, obj):
        return obj.city.name if obj.city_id else None


class LeafletCreateSerializer(serializers.ModelSerializer):
    pet = PetCreateSerializer()
    email = serializers.EmailField(source="creator.email")

    class Meta:
        model = Leaflet
        fields = ["id", "status", "pet", "city", "email"]

    def create(self, validated_data):
        print(validated_data)
        status = validated_data.get("status")
        pet_data = validated_data.get("pet")
        email = validated_data.pop("creator").get("email")
        user, created = User.objects.get_or_create(email=email)
        print(created, user)
        if created:
            password = User.objects.make_random_password()
            user.set_password(password)
            user.save()
            send_mail(
                "Ваш пароль к Petify",
                password,
                "django.test.emailservice@gmail.com",
                [email],
                fail_silently=False,
            )
        if status == "s":
            pet_data["owner"] = user
        else:
            pet_data["finder"] = user
        pet = Pet(**pet_data)
        pet.save()
        validated_data["pet"] = pet
        validated_data["creator"] = user
        return super().create(validated_data)
