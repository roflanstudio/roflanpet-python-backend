from django.contrib import admin

from .models import Leaflet

admin.site.register(Leaflet)