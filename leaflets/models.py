import requests
from django.db import models
from django.db.models.signals import pre_save, post_save

from pets.models import Pet
from accounts.models import User
from utils.models import City, HitCount

STATUSES = (
    ("s", "Ищу"),
    ("f", "Нашёл"),
)


class Leaflet(models.Model):
    resolved = models.BooleanField(default=False)
    status = models.CharField(max_length=1, choices=STATUSES)
    creator = models.ForeignKey(User, models.CASCADE, related_name="leaflets")
    pet = models.ForeignKey(Pet, models.CASCADE, related_name="leaflets")
    city = models.ForeignKey(City, models.CASCADE, related_name="leaflets")

    hit_counter = models.OneToOneField(
        HitCount, models.SET_NULL, null=True, blank=True, related_name="leaflets"
    )
    qr_counter = models.OneToOneField(
        HitCount, models.SET_NULL, null=True, blank=True, related_name="qr_leaflets"
    )

    date_created = models.DateTimeField(auto_now_add=True)


def pre_save_hit_counter(sender, instance, *args, **kwargs):
    if not instance.hit_counter:
        hit = HitCount()
        hit.save()
        instance.hit_counter = hit
    if not instance.qr_counter:
        qr = HitCount()
        qr.save()
        instance.qr_counter = qr


def post_save_request(sender, instance, *args, **kwargs):
    if kwargs.get("created"):
        requests.get(
            f"https://petify-spring.herokuapp.com/leaflet/get/matching?leafletId={instance.id}"
        )


pre_save.connect(pre_save_hit_counter, sender=Leaflet)
post_save.connect(post_save_request, sender=Leaflet)