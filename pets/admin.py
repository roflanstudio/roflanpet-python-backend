from django.contrib import admin

from .models import Species, Breed, Pet, PetImage


class BreedInlineAdmin(admin.StackedInline):
    model = Breed
    classes = ["collapse"]
    extra = 0


class PetImageInlineAdmin(admin.StackedInline):
    model = PetImage
    classes = ["collapse"]
    extra = 0


class SpeciesAdmin(admin.ModelAdmin):
    inlines = [BreedInlineAdmin]


class PetAdmin(admin.ModelAdmin):
    inlines = [PetImageInlineAdmin]


admin.site.register(Breed)
admin.site.register(Species, SpeciesAdmin)
admin.site.register(Pet, PetAdmin)