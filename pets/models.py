from django.db import models

from accounts.models import User


class Species(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.name


class Breed(models.Model):
    name = models.CharField(max_length=50)
    species = models.ForeignKey(Species, models.CASCADE, related_name="breeds")

    def __str__(self) -> str:
        return self.name


GENDERS = (
    ("m", "Женского пола"),
    ("f", "Мужского пола"),
)

SIZES = (
    ("t", "Очень маленький"),
    ("s", "Маленький"),
    ("m", "Средний"),
    ("l", "Большой"),
    ("h", "Огромный"),
)


class Pet(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    species = models.ForeignKey(Species, models.CASCADE, related_name="animals")
    breed = models.ForeignKey(
        Breed, models.CASCADE, related_name="animals", null=True, blank=True
    )
    gender = models.CharField(max_length=1, choices=GENDERS, null=True, blank=True)
    size = models.CharField(max_length=1, choices=SIZES, null=True, blank=True)
    color = models.CharField(max_length=20, null=True, blank=True)
    extras = models.TextField(null=True, blank=True)

    owner = models.ForeignKey(
        User, models.CASCADE, related_name="pets", null=True, blank=True
    )
    finder = models.ForeignKey(
        User, models.CASCADE, related_name="found_pets", null=True, blank=True
    )

    def __str__(self) -> str:
        return self.name if self.name else self.species.name


class PetImage(models.Model):
    pet = models.ForeignKey(Pet, models.CASCADE, related_name="images")
    pic = models.ImageField()

    def __str__(self):
        return "Image for " + str(self.pet)
