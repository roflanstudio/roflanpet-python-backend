from rest_framework import serializers

from .models import Pet, PetImage


class PetImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PetImage
        fields = ["pic"]


class PetImageAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = PetImage
        fields = ["pic", "pet"]


class PetSerializer(serializers.ModelSerializer):
    images = PetImageSerializer(many=True)
    species = serializers.CharField(source="species.name")
    breed = serializers.SerializerMethodField()
    size = serializers.CharField(source="get_size_display")
    gender = serializers.CharField(source="get_gender_display")

    class Meta:
        model = Pet
        fields = [
            "id",
            "name",
            "images",
            "species",
            "breed",
            "size",
            "gender",
            "color",
        ]

    def get_breed(self, obj):
        return obj.breed.name if obj.breed_id else None


class PetCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pet
        fields = [
            "id",
            "name",
            "species",
            "breed",
            "size",
            "gender",
            "color",
            "extras",
        ]