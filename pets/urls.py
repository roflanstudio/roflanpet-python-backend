from django.urls import path

from . import views

urlpatterns = [
    path("my/", views.MyPetsAPIView.as_view()),
    path("add-photo/", views.PetImageCreateAPIView.as_view()),
]
