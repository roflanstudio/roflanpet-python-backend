from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated

from . import models, serializers


class MyPetsAPIView(ListAPIView):
    serializer_class = serializers.PetSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return models.Pet.objects.filter(owner=self.request.user)


class PetImageCreateAPIView(CreateAPIView):
    serializer_class = serializers.PetImageAddSerializer
